FROM openjdk:8u111-jdk-alpine
EXPOSE 8081
VOLUME /tmp
ADD /build/libs/juegos-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]