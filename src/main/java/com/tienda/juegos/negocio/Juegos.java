package com.tienda.juegos.negocio;


import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="juegos")
@Access(AccessType.FIELD)

public class Juegos  {

	public Juegos() {
	}
	
	public Juegos(String name) {
		this.name = name;
	}	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique =true,nullable=false)
	public long id;
	
	@Column(name="name", nullable=false, length=255)
	private String name;	
	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	
}
