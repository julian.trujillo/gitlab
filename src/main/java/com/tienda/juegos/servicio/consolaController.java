package com.tienda.juegos.servicio;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.juegos.dao.ConsolaDAO;
import com.tienda.juegos.negocio.Consola;



@RestController
@RequestMapping("/consolas")
public class consolaController {
	@Autowired
	private ConsolaDAO consoladao;

	//GetMapping All
	@GetMapping
	public ResponseEntity<List<Consola>> getConsola(){
		List<Consola> consolas= consoladao.findAll();
		return ResponseEntity.ok(consolas);
			
		 	
	}
	//Get by id
	@RequestMapping(value="{consoleid}")
	public ResponseEntity<Consola> getConsolaById(@PathVariable("consoleid") Long consoleid){
		Optional<Consola> optionalConsola = consoladao.findById(consoleid);
		if(optionalConsola.isPresent()) {
			return ResponseEntity.ok(optionalConsola.get());
			
		}
		else{
			return ResponseEntity.noContent().build();
		}
	}
	// POST New console
	@PostMapping
	public ResponseEntity<Consola> createConsole(@RequestBody Consola consola){
		
		Consola newconsola= consoladao.save(consola);
		return ResponseEntity.ok(newconsola);
	
	}
	
	//Delete console
	
	@DeleteMapping(value="{consoleid}")
	public ResponseEntity<Void> deleteConsole(@PathVariable("consoleid") Long consoleid){
		
		consoladao.deleteById(consoleid);
		return ResponseEntity.ok(null);
	
	}
	
	//Update console
	@PutMapping
	public ResponseEntity<Consola> updateConsola(@RequestBody Consola consola){
		Optional<Consola> optionalConsola = consoladao.findById(consola.getId());
		if(optionalConsola.isPresent()) {
			Consola updateConsola = optionalConsola.get();
			updateConsola.setName(consola.getName());
			consoladao.save(updateConsola);
			return ResponseEntity.ok(updateConsola);
		}
		else{
			return ResponseEntity.notFound().build();
		}
	}
	
	
}
