package com.tienda.juegos.servicio;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.juegos.dao.IJuegoDAO;
import com.tienda.juegos.negocio.Juegos;


@CrossOrigin(origins = "*")
@RestController
public class juegoController {
	
	
	private IJuegoDAO juegoDAO;


	@RequestMapping(value="/juegos", method= RequestMethod.GET)
	public List<Juegos> index() {
		return this.juegoDAO.findAll();
	}
	

	
}
