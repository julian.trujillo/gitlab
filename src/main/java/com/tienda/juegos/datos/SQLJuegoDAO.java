package com.tienda.juegos.datos;

import java.util.List;

import com.tienda.juegos.dao.IJuegoDAO;
import com.tienda.juegos.negocio.Juegos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class SQLJuegoDAO implements IJuegoDAO{

	private static final Logger LOG = LoggerFactory.getLogger(SQLJuegoDAO.class);
    private static final String SQL_EXCEPTION_STRING = "SQL exception: {}.";
    private javax.sql.DataSource dataSource;


    @Autowired
    public SQLJuegoDAO(javax.sql.DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Juegos> findAll() {
        LOG.info("Getting juego list!");
        List<Juegos> juego = new ArrayList<>();
        try (Connection connection = this.dataSource.getConnection()) {
            String query = "SELECT * FROM juegos";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                try (ResultSet result = preparedStatement.executeQuery()) {
                    while (result.next()) {
                    	juego.add(this.buildjuego(result));
                    }
                }
            }
        } catch (SQLException e) {
            LOG.error(SQL_EXCEPTION_STRING, e.getMessage());
        }
        return juego;
    }
    
    private Juegos buildjuego(ResultSet resultSet) throws SQLException {
    	Juegos juego = new Juegos();
    	juego.setId(resultSet.getLong(1));
    	juego.setName(resultSet.getString(2));
        return juego;
    }
    
    
	
}
